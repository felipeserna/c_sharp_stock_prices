﻿using Newtonsoft.Json;
using System.Net;
namespace TaskWinFormsApp;
public class StockPrice
{
    public string Identifier { get; set; } = null!;
    public DateTime TradeDate { get; set; }
    public decimal? Open { get; set; }
    public decimal? High { get; set; }
    public decimal? Low { get; set; }
    public decimal? Close { get; set; }
    public int Volume { get; set; }
    public decimal Change { get; set; }
    public decimal ChangePercent { get; set; }

    public static string API_URL = "https://ps-async.fekberg.com/api/stocks/MSFT";
    
    public async Task GetStocksAsync()
    {
        await Task.Delay(4000);

        using (var client = new HttpClient())
        {
            var responseTask = client.GetAsync($"{API_URL}");
            var response = await responseTask;

            var content = await response.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<IEnumerable<StockPrice>>(content);
                        
            var MaxChange = data.Max(x => x.Change);
            Console.WriteLine($"MSFT Max change {MaxChange}");
        }
    }

    public static string API_URL_2 = "https://ps-async.fekberg.com/api/stocks/Googl";

    public async Task GetStocksAsync_2()
    {
        await Task.Delay(4000);

        using (var client = new HttpClient())
        {
            var responseTask = client.GetAsync($"{API_URL_2}");
            var response = await responseTask;

            var content = await response.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<IEnumerable<StockPrice>>(content);

            var MinChangePercent = data.Min(x => x.ChangePercent);
            Console.WriteLine($"Googl Min ChangePercent {MinChangePercent}");
        }
    }

    public static string API_URL_3 = "https://ps-async.fekberg.com/api/stocks/Voya";

    public async Task GetStocksAsync_3()
    {
        await Task.Delay(4000);

        using (var client = new HttpClient())
        {
            var responseTask = client.GetAsync($"{API_URL_3}");
            var response = await responseTask;

            var content = await response.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<IEnumerable<StockPrice>>(content);

            var AverageVolume = data.Average(x => x.Volume);
            Console.WriteLine($"Voya Average Volume {AverageVolume}");
        }
    }

    public static string API_URL_4 = "https://ps-async.fekberg.com/api/stocks/Hbi";

    public async Task GetStocksAsync_4()
    {
        await Task.Delay(4000);

        using (var client = new HttpClient())
        {
            var responseTask = client.GetAsync($"{API_URL_4}");
            var response = await responseTask;

            var content = await response.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<IEnumerable<StockPrice>>(content);

            var NumOfRecords = data.Count();
            Console.WriteLine($"Hbi Num of records {NumOfRecords}");
        }
    }

    public static string API_URL_5 = "https://ps-async.fekberg.com/api/stocks/Foxf";

    public async Task GetStocksAsync_5()
    {
        await Task.Delay(4000);

        using (var client = new HttpClient())
        {
            var responseTask = client.GetAsync($"{API_URL_5}");
            var response = await responseTask;

            var content = await response.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<IEnumerable<StockPrice>>(content);

            var MinChange = data.Min(x => x.Change);
            Console.WriteLine($"Foxf Min change {MinChange}");
        }
    }
}